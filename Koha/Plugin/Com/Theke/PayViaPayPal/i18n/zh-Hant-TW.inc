/*
	[%
		TOKENS = {
			home_breadcrumb = "首頁"
			your_payment_breadcrumb = "Your Payment"
			error = "錯誤"
			error_title = "處理您的支付有問題"
			PAYPAL_UNABLE_TO_CONNECT = "不能連結至 PayPal。"
			PAYPAL_TRY_LATER = "請稍後再試。"
			PAYPAL_ERROR_PROCESSING = "不能確認付款。"
			PAYPAL_CONTACT_LIBRARY = "請與圖書館連繫確認您的支付。"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "回到罰款詳情"
		}
	%]
*/