/*
	[%
		TOKENS = {
			home_breadcrumb = "Hasiera"
			your_payment_breadcrumb = "Your Payment"
			error = "Error"
			error_title = "Zure ordainketarekin arazo bat egon da"
			PAYPAL_UNABLE_TO_CONNECT = "Ezin da PayPal-era konektatu."
			PAYPAL_TRY_LATER = "Mesedez, saiatu berriz beranduago."
			PAYPAL_ERROR_PROCESSING = "Ezin da ordainketa egiaztatu."
			PAYPAL_CONTACT_LIBRARY = "Mesedez, jarri liburutegiarekin harremanetan ordainketa baieztatzeko."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Isunaren xehetasunetara itzuli"
		}
	%]
*/