/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "Ralat:"
			error_title = "Terdapat masalah dengan penyerahan anda"
			PAYPAL_UNABLE_TO_CONNECT = "Tag gagal ditambah."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Tag gagal ditambah."
			PAYPAL_CONTACT_LIBRARY = ". Sila hubungi perpustakaan untuk maklumat lanjut."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Pulangkan item ini "
		}
	%]
*/