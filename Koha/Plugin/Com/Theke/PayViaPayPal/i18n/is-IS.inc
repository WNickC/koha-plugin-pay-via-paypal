/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "Villa:"
			error_title = "Það komu upp vandarmál með sendinguna"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "Vara tengiliðaupplýsingar"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Skila þessum hlut "
		}
	%]
*/