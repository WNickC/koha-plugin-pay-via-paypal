/*
	[%
		TOKENS = {
			home_breadcrumb = "Домівка"
			your_payment_breadcrumb = "Your Payment"
			error = "Помилка"
			error_title = "виникла проблема при обробці Вашого платежу"
			PAYPAL_UNABLE_TO_CONNECT = "Неможливо з’єднатися з PayPal."
			PAYPAL_TRY_LATER = "Спробуйте ще раз пізніше."
			PAYPAL_ERROR_PROCESSING = "Неможливо перевірити платіж."
			PAYPAL_CONTACT_LIBRARY = "Будь ласка, зверніться до бібліотеки для підтвердження Вашої оплати."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Повернутися до подробиць про пені"
		}
	%]
*/