/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "ข้อผิดพลาด:"
			error_title = "มีปัญหากับการส่งข้อมูลของคุณ"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "ข้อมูลติดต่อ"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "คืนรายการนี้ "
		}
	%]
*/